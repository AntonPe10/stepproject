const servicesBtn = document.querySelectorAll(".nav-services li");
const servicesBox = document.querySelectorAll(".services-blok");

servicesBtn.forEach((item, liIndex) => {
    item.addEventListener('click', (e) => {
        e.preventDefault();
        clineSelected(servicesBtn);
        clineSelectedBox(servicesBox, liIndex);
        item.classList = "selected"
    });
});

function clineSelected(arr) {
    arr.forEach(item => {
        item.classList = "";
    })
}

function clineSelectedBox(arr, liIndex) {
    arr.forEach((item, BoxIndex) => {
        item.classList = "services-blok";
        if (liIndex === BoxIndex) {
            item.classList = "services-blok selected";
        }
    })
}
function clineSelectedBox2(arr, Liindex) {
    arr.forEach((item, boxIndex) => {
        item.classList = "grid img-box";
        if (Liindex === boxIndex) {
            item.classList = "grid img-box selected"
        }
    })
}
const servicesBtn2 = document.querySelectorAll(".nav-services2 li");
const sevicesBox2 = document.querySelectorAll(".img-box");

servicesBtn2.forEach((item, liIndex) => {
    item.addEventListener("click", (e) => {
        e.preventDefault();
        clineSelected(servicesBtn2);
        clineSelectedBox2(sevicesBox2, liIndex);
        item.classList = "selected"
    })
});

// get all images
const allImagesBlock = document.querySelectorAll('.allImagesHiddenBox img');
const allImages = [];

const allLoadImagesBox = document.querySelectorAll('.allImagesHiddenBoxLoad img');
const allLoadImages = [];

allLoadImagesBox.forEach(img => {
    const obj = {
        src: img.src,
        category: img.dataset.category,
    };
    allLoadImages.push(obj);
});

allImagesBlock.forEach(img => {
    const obj = {
        src: img.src,
        category: img.dataset.category,
    };
    allImages.push(obj);
});

// manage all images to blocks
const allBoxes = document.querySelectorAll('.img-box');

const insertImage = (img, box) => {
    const image = document.createElement('img');
    image.src = img.src;
    box.appendChild(image);
};

allBoxes.forEach(box => {
    const category = box.dataset.category;
    if (category === 'all') {
        allImages.forEach(img => {
            insertImage(img, box);
        });
        return;
    }
    const filtered = allImages.filter(img => img.category === category);
    filtered.forEach(img => {
        insertImage(img, box)
    });
});

// load button
const loadMoreBtn = document.querySelector('.btn-load');
const preloader = document.querySelector('.preloader');
let loadMoreBtnCount = 0;

function loadMoreFunc() {
    loadMoreBtnCount++;
    if (loadMoreBtnCount >= 3) {
        loadMoreBtn.style = 'display: none;';
        return;
    }
    loadMoreBtn.style = 'display: none;';
    preloader.style = 'display: block;';
    setTimeout(() => {
        loadMoreBtn.style = 'display: block;';
        preloader.style = 'display: none;'
    }, 2000);
    allBoxes.forEach(box => {
        const category = box.dataset.category;
        if (category === 'all') {
            allLoadImages.forEach(img => {
                insertImage(img, box);
            })
        }
    })
}

loadMoreBtn.addEventListener('click', loadMoreFunc);


const leftBtn = document.querySelector('.left-btn');
const rightBtn = document.querySelector('.right-btn');
const  boxUserItems = document.querySelectorAll('.box-user-item');
const avas = document.querySelectorAll('.carousel .ava');

function carouselClick (dir) {
    let activeBoxIndex = null;
    boxUserItems.forEach((box, indx)=> {
        if (box.classList.value === 'box-user-item active') {
            activeBoxIndex = indx;
        }
        box.classList = 'box-user-item';
        avas[indx].classList = 'ava';
    });
    if (dir === 'left') {
        if (activeBoxIndex === 0) {
            activeBoxIndex = boxUserItems.length;
        }
        activeBoxIndex = activeBoxIndex - 1;
    } else if (dir === 'right') {
        activeBoxIndex = activeBoxIndex + 1;
    }
    if (boxUserItems[activeBoxIndex]) {
        boxUserItems[activeBoxIndex].classList = 'box-user-item active';
        avas[activeBoxIndex].classList = 'ava active';
    } else {
        boxUserItems[0].classList = 'box-user-item active';
        avas[0].classList = 'ava active';
    }
};
leftBtn.addEventListener('click', () => carouselClick('left'));
rightBtn.addEventListener('click', () => carouselClick('right'));



